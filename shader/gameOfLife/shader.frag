#version 330
in vec2 uv;
out vec4 frag_color;
uniform sampler2D texture;

void main()
{
    int r00 = int(textureOffset(texture, uv, ivec2(-1, 1)).r);
    int r01 = int(textureOffset(texture, uv, ivec2( 0, 1)).r);
    int r02 = int(textureOffset(texture, uv, ivec2( 1, 1)).r);

    int r10 = int(textureOffset(texture, uv, ivec2(-1, 0)).r);
    int r11 = int(texture(texture, uv).r);
    int r12 = int(textureOffset(texture, uv, ivec2( 1, 0)).r);

    int r20 = int(textureOffset(texture, uv, ivec2(-1,-1)).r);
    int r21 = int(textureOffset(texture, uv, ivec2( 0,-1)).r);
    int r22 = int(textureOffset(texture, uv, ivec2( 1,-1)).r);

    int finalSum = (r00 + r10 + r20) +
                   (r01 + r11 + r21) +
                   (r02 + r12 + r22);

    if(finalSum == 3)
            frag_color = vec4(1,1,1,1);
    else if(finalSum == 4 && r11 == 1)
            frag_color = vec4(1,1,1,1);
    else
            frag_color = vec4(0,0,0,1);

}