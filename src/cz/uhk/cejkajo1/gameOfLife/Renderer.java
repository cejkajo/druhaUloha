package cz.uhk.cejkajo1.gameOfLife;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import utils.*;
import java.awt.event.*;

/**
 * Implementace algoritmu pro buněčný automat hra Life
 * @author cejkajo1
 */

public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height, ox, oy;

	OGLBuffers buffers;

	int shaderProgram;

	OGLTexture dataTexture;
	
	OGLRenderTarget renderTarget;
	OGLRenderTarget renderTarget2;
	OGLRenderTarget renderTargetHlp;
	
	boolean init = true;
	OGLTexImageFloat dataTexImage = null;
	int dataWidth = 512, dataHeight = 512;
	
	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();

		System.out.println("Init GL is " + gl.getClass().getName());
		System.out.println("OpenGL version " + gl.glGetString(GL2.GL_VERSION));
		System.out.println("OpenGL vendor " + gl.glGetString(GL2.GL_VENDOR));
		System.out
				.println("OpenGL renderer " + gl.glGetString(GL2.GL_RENDERER));
		System.out.println("OpenGL extensions "
				+ gl.glGetString(GL2.GL_EXTENSIONS));

		shaderProgram = ShaderUtils.loadProgram(gl, "./shader/gameOfLife/shader");

		createBuffers(gl);

		//dva renderTargets, z jednoho se bude cist a do druheho rendrovat
		renderTarget = new OGLRenderTarget(gl, dataWidth, dataHeight);
		renderTarget2 = new OGLRenderTarget(gl, dataWidth, dataHeight);

		gl.glDisable(GL2.GL_DEPTH_TEST);
	
	}

	void initData(GL2 gl) {
		// vytvorime pole hodnot
		dataTexImage = new OGLTexImageFloat(dataWidth, dataHeight, 4);
		for (int i = 0; i < dataHeight; i++){
			dataTexImage.setPixel(i, i, 0, 1.0f);
		}	
		// vytvorime texturu
		dataTexture = new OGLTexture(gl, dataTexImage);
	}

	void createBuffers(GL2 gl) {
		//quad pres celou obrazovku, staci pozice, texCoord lze dopocitat
		float[] quad = { 1, -1, 1, 1, -1, 1, -1, -1 };
		
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2)}; // nastavim atribut

		buffers = new OGLBuffers(gl, quad, attributes, null); // poslu pozice quadu
	}

	public void display(GLAutoDrawable glDrawable) {
		
		GL2 gl = glDrawable.getGL().getGL2();

		// rendrujeme do renderTarget, ne na obrazovku
		renderTarget.bind();

		gl.glUseProgram(shaderProgram);
		gl.glClearColor(0.5f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT );
		
		if (init) {
			init = false;
			initData(gl);
		} else {
			renderTarget2.getColorTexture().bind(shaderProgram, "texture", 0);
		}
		
		buffers.draw(GL2.GL_QUADS, shaderProgram);

		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
		gl.glViewport(0, 0, width, height);

		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);

		buffers.draw(GL2.GL_QUADS, shaderProgram);

		//prehozeni renderTargetu
		renderTargetHlp = renderTarget2;
		renderTarget2 = renderTarget;
		renderTarget = renderTargetHlp;
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_I:
			init = true;;
			break;
	
		}
	}
	
	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable arg0) {
	}
}